//
//  InfoDetails.swift
//  PACTERATestProject
//
//  Created by Jiaren on 1/12/2016.
//  Copyright © 2016 Jiaren. All rights reserved.
//

import UIKit

class InfoDetails: NSObject {
    var infoTitle = ""
    var infoDesc = ""
    var infoURL = ""
    
    init(infoTitle:String, infoDesc:String, infoURL:String) {
        self.infoTitle = infoTitle
        self.infoDesc = infoDesc
        self.infoURL = infoURL
    }
}
