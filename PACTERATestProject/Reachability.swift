//
//  Reachability.swift
//  Offline Charts
//
//  Created by Jiaren on 02/12/2016.
//  Copyright © 2016 Bamuda Corporation. All rights reserved.
//


import AVFoundation
import SystemConfiguration

public class Reachability {
    
    class func isConnectedToNetwork(resultHandler: @escaping (_ isTrue: Bool) -> Void) {
        let defaults = UserDefaults.standard
        
        
        let urlA = NSURL(string: "http://www.google.com/")
        let urlB = NSURL(string: "http://www.baidu.com/")
        
        if let _ = urlA {
            //Do nothing
        } else {
            resultHandler(false)
        }
        if let _ = urlB {
            //Do nothing
        } else {
            resultHandler(false)
        }
        
        let urlDict:[String:NSURL] = ["A":urlA!, "B":urlB!]
        
        var preference = "A"
        
        defaults.synchronize()
        if let temp = defaults.string(forKey: "preferredNetworkURL") {
            preference = temp //check the last successful URL or A by default
        } else {
            defaults.set(preference, forKey: "preferredNetworkURL")
            defaults.synchronize()
        }
        
        Reachability.fetchURL(url: urlDict[preference]!, pref:preference) {
            isTrue in
            if isTrue == true {
                print("NETWORK STATUS: \(isTrue)")
                resultHandler(isTrue)
            } else {
                //check the URL which hasn't been checked
                if preference == "A" {
                    preference = "B"
                } else {
                    preference = "A"
                }
                Reachability.fetchURL(url: urlDict[preference]!, pref:preference) {
                    isTrue in
                    print("NETWORK STATUS: \(isTrue)")
                    resultHandler(isTrue)
                }
                //if preference "B" returns true, then Baidu.com is available and user is likely in a country where Google is blocked
            }
        }
    }
    
    class func fetchURL(url:NSURL, pref:String, resultHandler: @escaping (_ isTrue: Bool) -> Void) {
        print("URL PREFERENCE: \(pref)")
        let defaults = UserDefaults.standard
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "HEAD"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        
        var returnBool = false
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    defaults.set(pref, forKey: "preferredNetworkURL") //remember the URL that succeeded and check it first next time
                    returnBool = true
                    resultHandler(returnBool)
                } else {
                    returnBool = false
                    resultHandler(returnBool)
                }
            } else {
                returnBool = false
                resultHandler(returnBool)
            }
        }
        dataTask.resume()
        session.finishTasksAndInvalidate()
    }
    
}
