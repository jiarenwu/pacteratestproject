//
//  InfoCell.swift
//  PACTERATestProject
//
//  Created by Jiaren on 1/12/2016.
//  Copyright © 2016 Jiaren. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var imgWidthConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var descTrailingToImgConstraints: NSLayoutConstraint!
}
