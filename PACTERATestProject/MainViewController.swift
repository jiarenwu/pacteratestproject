//
//  MainViewController.swift
//  PACTERATestProject
//
//  Created by Jiaren on 1/12/2016.
//  Copyright © 2016 Jiaren. All rights reserved.
//

import UIKit
import Foundation


class MainViewController: UITableViewController {

    var jsonItems = [InfoDetails]()
    let defaultImg = UIImage(named: "defaultImg")
    var currentImageUrls = [String]()
    var imagesDict = [String : UIImage]()
    
    var task: URLSessionDownloadTask!
    var session: URLSession!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Pactera TestProject"
        // Uncomment the following line to preserve selection between presentations
        session = URLSession.shared
        task = URLSessionDownloadTask()
        
        //pull down table cell to refresh
        let refreshCtrl = UIRefreshControl()
        refreshCtrl.addTarget(self, action: #selector(refreshFeed), for: .valueChanged)
        self.refreshControl = refreshCtrl
        
        //In case user doesn't pull the table, she/he could tap the button to refresh
        let refeshScreenBtn = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(refreshFeed))
        self.navigationItem.rightBarButtonItem = refeshScreenBtn
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationDidChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        //Automatically calculate cell height
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        self.refreshFeed()
    }

    func downloadConent()  {
        var sessionConfiguration:URLSessionConfiguration? = nil
        sessionConfiguration = URLSessionConfiguration.default
        
        sessionConfiguration!.allowsCellularAccess = true
        let session = Foundation.URLSession(configuration: sessionConfiguration!, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL(string: kJsonFeedURLString)!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest) {(myData, response, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            let statusCode = (response as! HTTPURLResponse).statusCode
            print("Success: \(statusCode)")
            if let data = myData {
                var jsonDictTmp:[String:AnyObject]?
                
                //As some characters couldn't be converted from data to string, need to convert to tmp variable first
                let tmpString = String(data: data, encoding: String.Encoding.isoLatin1)
                
                let tmpData = tmpString?.data(using: String.Encoding.utf8)
                
                do {
                    jsonDictTmp = try JSONSerialization.jsonObject(with: tmpData!, options: []) as? [String:AnyObject]
                } catch {
                    print("Error when getting JSON Object")
                }
                if let jsonDict = jsonDictTmp {
                    
                    if let rowsArray = jsonDict["rows"] as? [[String : AnyObject]] {
                        self.jsonItems.removeAll()
                        self.currentImageUrls.removeAll()
                        
                        for row in rowsArray {
                            let title = (row["title"] as? String) ?? ""
                            let description = (row["description"] as? String) ?? ""
                            let imageURL = (row["imageHref"] as? String) ?? ""
                            if imageURL != "" {
                                self.currentImageUrls.append(imageURL)
                            }
                            let details = InfoDetails(infoTitle: title, infoDesc: description, infoURL: imageURL)
                            self.jsonItems.append(details)
                        }
                        
                    }
                    /* Delete non existing objects */
                    let tmpDict = self.imagesDict
                    for (key,_) in tmpDict {
                        if !self.currentImageUrls.contains(key){
                            self.imagesDict[key] = nil
                        }
                    }
                    DispatchQueue.main.sync(execute: {
                        self.title = jsonDict["title"] as? String //Update navigator bar title
                        self.tableView.reloadData()
                    })
                }
            }
            
        }
        
        task.resume()
        session.finishTasksAndInvalidate()
    }
    func refreshFeed()  {
        Reachability.isConnectedToNetwork(){
            isConnected in
            

            if(isConnected){
                self.downloadConent()
            } else {
                DispatchQueue.main.async {
                    let alert = UIAlertView(title: "Lost Connection", message: "No Internet connection. Coudn't refresh.", delegate: nil, cancelButtonTitle: NSLocalizedString("OK",comment:""))
                    
                    alert.show()
                }
            }
        }
     }
    
    func deviceOrientationDidChange(_ notification:NSNotification) {
        self.tableView.reloadData()
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell") as! InfoCell
        
        let infoDetails = jsonItems[indexPath.row]
 
        cell.titleLbl.text = infoDetails.infoTitle
        cell.descLbl.text = infoDetails.infoDesc
        
        if infoDetails.infoURL.characters.count > 0 {
            cell.imgView?.image = defaultImg
            let urlString = infoDetails.infoURL
            // If this image is already cached, don't re-download
            if let img = imagesDict[urlString] {
                cell.imgView?.image = img
            }
            else {
                // The image isn't cached, download the img data
                if let url = URL(string: urlString) {
                    task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                        if let data = try? Data(contentsOf: url){
                            // Convert the downloaded data in to a UIImage object
                            let image = UIImage(data: data)
                            // Store the image in to our cache
                            self.imagesDict[urlString] = image
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                if let cellToUpdate = tableView.cellForRow(at: indexPath) as? InfoCell {
                                    cellToUpdate.imgView?.image = image
                                }
                            })
                        }
                    })
                    task.resume()
                }
            }
            cell.imgWidthConstraints.constant = 150
            cell.descTrailingToImgConstraints.constant = 8
        } else {
            //No image
            cell.imgView.image = nil;
            cell.imgWidthConstraints.constant = 0
            cell.descTrailingToImgConstraints.constant = -5
        }
        cell.layoutIfNeeded()
        return cell
    }
}
